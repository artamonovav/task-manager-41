-- Database: tm

-- DROP DATABASE IF EXISTS tm;

CREATE DATABASE tm
    WITH 
    OWNER = tm
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE tm
    IS 'task manager database';