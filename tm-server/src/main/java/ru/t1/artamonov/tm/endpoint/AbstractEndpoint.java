package ru.t1.artamonov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.service.IServiceLocator;
import ru.t1.artamonov.tm.dto.model.SessionDTO;
import ru.t1.artamonov.tm.dto.request.AbstractUserRequest;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.exception.user.AccessDeniedException;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Nullable
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @SneakyThrows
    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final SessionDTO session = getServiceLocator().getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    @SneakyThrows
    protected SessionDTO check(
            @Nullable final AbstractUserRequest request
    ) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return getServiceLocator().getAuthService().validateToken(token);
    }

}
