package ru.t1.artamonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (row_id, created, name, descrptn, status, user_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId});")
    void add(@NotNull ProjectDTO model);

    @Delete("TRUNCATE TABLE tm_project;")
    void clearAll();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId};")
    void clear(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT COUNT(1) = 1 FROM tm_project WHERE row_id = #{id};")
    Boolean existsById(@Param("id") @NotNull String id);

    @NotNull
    @Select("SELECT COUNT(1) = 1 FROM tm_project WHERE row_id = #{id} AND user_id = #{userId};")
    Boolean existsByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT * FROM tm_project;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<ProjectDTO> findAll();

    @Nullable
    @Select("SELECT * FROM tm_project ORDER BY name;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")

    })
    List<ProjectDTO> findAllOrderByName();

    @Nullable
    @Select("SELECT * FROM tm_project ORDER BY created;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<ProjectDTO> findAllOrderByCreated();

    @Nullable
    @Select("SELECT * FROM tm_project ORDER BY status;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<ProjectDTO> findAllOrderByStatus();

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<ProjectDTO> findAllUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<ProjectDTO> findAllOrderByNameUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<ProjectDTO> findAllOrderByCreatedUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<ProjectDTO> findAllOrderByStatusUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * from tm_project WHERE row_id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    ProjectDTO findOneById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT * from tm_project WHERE row_id = #{id} AND user_id = #{userId}  LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn")
    })
    ProjectDTO findOneByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Select("SELECT COUNT(1) FROM tm_project;")
    long getSize();

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId};")
    long getSizeUserId(@Param("userId") @Nullable String userId);

    @Delete("DELETE FROM tm_project WHERE row_id = #{id};")
    void remove(@NotNull ProjectDTO model);

    @Delete("DELETE FROM tm_project WHERE row_id = #{id} and user_id = #{userId};")
    void removeUserId(@Param("userId") @Nullable String userId, @Nullable ProjectDTO model);

    @Delete("DELETE FROM tm_project WHERE row_id = #{id};")
    void removeById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_project WHERE row_id = #{id} and user_id = #{userId};")
    void removeByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Update("UPDATE tm_project SET created = #{created}, name = #{name}, descrptn = #{description}, status = #{status}, user_id = #{userId} WHERE row_id = #{id};")
    void update(@NotNull ProjectDTO model);

}
